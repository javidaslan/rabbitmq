import pika
import json
import time

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()
channel.queue_declare(queue='hello')
counter = 0
while counter < 5:
    data = json.dumps({'name': 'server1', 'ip': '1.1.1.1'})
    channel.basic_publish(exchange='', routing_key='hello', body=data)
    counter += 1
    print("[x] Sent 'Hello World!'")
    time.sleep(1)

connection.close()